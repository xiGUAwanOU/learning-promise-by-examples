// Output order tested in node.js(v15), chrome and firefox

const thePromise = new Promise((resolve) => {
  console.log('output 1');
  setTimeout(() => {
    console.log('output 3');
    resolve();
  }, 500);
});

console.log('output 2');

thePromise.then(() => {
  console.log('output 4');
});
